package ru.kamalova.quest2;

public class Quest_2 {

    public String doubleChar(String s){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            sb.append(s.charAt(i)).append(s.charAt(i));
        }
        return sb.toString();
    }
}
