package ru.kamalova.quest4;

public class Quest_4 {

    public int loneSum(int a, int b, int c) {
        int sum = a + b + c;
        if (a == b) {
            sum -= a + b;
        }
        if (a == c) {
            sum -= a + c;
        }
        if (b == c) {
            sum -= b + c;
        }
        if ((a == b) && (b == c)) {
            sum = 0;
        }

        return sum;
    }
}
