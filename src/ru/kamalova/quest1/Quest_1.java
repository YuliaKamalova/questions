package ru.kamalova.quest1;

public class Quest_1 {

    public String twoChar(String s, int index) {
        if ((index < 0) || ((s.length() - 1) <= index))
            index = 0;
        return s.substring(index, index + 2);
    }
}
