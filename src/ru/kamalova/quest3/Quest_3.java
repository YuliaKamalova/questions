package ru.kamalova.quest3;

public class Quest_3 {

    public boolean xyBalanse(String s) {
        boolean x = false, y = false;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'x' && y) {
                x = true;
                y = false;
            } else if (s.charAt(i) == 'x') {
                x = true;
            }
            if (s.charAt(i) == 'y' && x)
                y = true;
        }
        if (!x) y = true;
        return y;
    }
}
