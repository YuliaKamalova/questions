package ru.kamalova.quest5;

public class Quest_5 {

    public boolean linearIn(int[] outer, int[] inner) {
        for (int anInner : inner) {
            if (!contains(anInner, outer)) return false;
        }
        return true;
    }

    private boolean contains(int x, int[] arr) {
        for (int anArr : arr) {
            if (anArr == x) return true;
        }
        return false;
    }
}
