package ru.kamalova.quest8;

import java.util.HashMap;
import java.util.Map;

public class Quest_8 {

    public String[] allSwap(final String[] words) {
        Map<Character, Integer> potentialSwap = new HashMap<>();

        for (int thisIndex = 0; thisIndex < words.length; thisIndex++) {
            if (words[thisIndex].isEmpty()) continue;

            Character firstChar = words[thisIndex].charAt(0);
            Integer potentialIndex = potentialSwap.remove(firstChar);

            if (potentialIndex != null) {
                int thatIndex = potentialIndex;
                String temp = words[thatIndex];
                words[thatIndex] = words[thisIndex];
                words[thisIndex] = temp;
            } else {
                potentialSwap.put(firstChar, thisIndex);
            }
        }

        return words;
    }

}
