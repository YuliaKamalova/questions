package ru.kamalova;

import ru.kamalova.quest1.Quest_1;
import ru.kamalova.quest2.Quest_2;
import ru.kamalova.quest3.Quest_3;
import ru.kamalova.quest4.Quest_4;
import ru.kamalova.quest5.Quest_5;
import ru.kamalova.quest6.Quest_6;
import ru.kamalova.quest7.Quest_7;
import ru.kamalova.quest8.Quest_8;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Q U E S T 1");
        Quest_1 quest1 = new Quest_1();
        String s = "Kamalova";
        System.out.println(quest1.twoChar(s, 0));
        System.out.println(quest1.twoChar(s, 2));
        System.out.println(quest1.twoChar(s, 7));
        System.out.println(quest1.twoChar(s, 6));
        System.out.println("---------------------------------------------------------------");

        System.out.println("Q U E S T 2");
        Quest_2 quest2 = new Quest_2();
        System.out.println(quest2.doubleChar(s));
        System.out.println("---------------------------------------------------------------");

        Quest_3 quest3 = new Quest_3();
        System.out.println(quest3.xyBalanse("aaxbby"));
        System.out.println(quest3.xyBalanse("aaxbb"));
        System.out.println(quest3.xyBalanse("yaaxbb"));
        System.out.println("---------------------------------------------------------------");

        System.out.println("Q U E S T 4");
        Quest_4 quest4 = new Quest_4();
        System.out.println(quest4.loneSum(1, 2, 3));
        System.out.println(quest4.loneSum(3, 2, 3));
        System.out.println(quest4.loneSum(3, 3, 3));
        System.out.println("---------------------------------------------------------------");

        System.out.println("Q U E S T 5");
        Quest_5 quest5 = new Quest_5();
        System.out.println(quest5.linearIn(new int[]{1, 2, 4, 6}, new int[]{2, 4}));
        System.out.println(quest5.linearIn(new int[]{1, 2, 4, 6}, new int[]{2, 3, 4}));
        System.out.println(quest5.linearIn(new int[]{1, 2, 4, 4, 6}, new int[]{2, 4}));
        System.out.println("---------------------------------------------------------------");

        System.out.println("Q U E S T 6");
        Quest_6 quest6 = new Quest_6();
        System.out.println(quest6.maxMirror(new int[]{1, 2, 3, 8, 9, 3, 2, 1}));
        System.out.println(quest6.maxMirror(new int[]{1, 2, 1, 4}));
        System.out.println(quest6.maxMirror(new int[]{7, 1, 2, 9, 7, 2, 1}));
        System.out.println("---------------------------------------------------------------");

        System.out.println("Q U E S T 7");
        Quest_7 quest7 = new Quest_7();
        System.out.println(Arrays.toString(quest7.fix45(new int[]{5, 4, 9, 4, 9, 5})));
        System.out.println(Arrays.toString(quest7.fix45(new int[]{1, 4, 1, 5})));
        System.out.println(Arrays.toString(quest7.fix45(new int[]{1, 4, 1, 5, 5, 4, 1})));
        System.out.println("---------------------------------------------------------------");

        System.out.println("Q U E S T 8");
        Quest_8 quest8 = new Quest_8();
        System.out.println(Arrays.toString(quest8.allSwap(new String[]{"ab", "ac"})));
        System.out.println(Arrays.toString(quest8.allSwap(new String[]{"ax", "bx", "cx", "cy", "by", "ay", "aaa", "azz"})));
        System.out.println(Arrays.toString(quest8.allSwap(new String[]{"ax", "bx", "ay", "by", "ai", "aj", "bx", "by"})));
        System.out.println("---------------------------------------------------------------");
    }
}
